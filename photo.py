import os, random, string, shutil

images = os.listdir('social')
for image in images:
    if os.path.exists('social/' + image):
        size = os.path.getsize('social/' + image)
        print image + ": " + str(size)

        folder = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))
        folder = "new/" + folder
        for image_file in images:
            if os.path.exists('social/' + image_file):
                s = os.path.getsize('social/' + image_file)
                if s == size and image != image_file:
                    print image + " = " + image_file
                    if not os.path.exists(folder):
                        os.makedirs(folder)
                        print folder + " Created"
                    shutil.move("social/" + image_file, folder + "/" + image_file)
