import io
import os, time

# Imports the Google Cloud client library
from google.cloud import vision

# Instantiates a client
vision_client = vision.Client()

images = os.listdir('social')
for image in images:
    time.sleep(10)
    image_path = 'social/' + image
    # The name of the image file to annotate
    file_name = os.path.join(
        os.path.dirname(__file__),
        image_path)

    # Loads the image into memory
    with io.open(file_name, 'rb') as image_file:
        content = image_file.read()
        image = vision_client.image(
            content=content)

    # Performs label detection on the image file
    labels = image.detect_labels()

    f = open("meta.txt","w")
    for label in labels:
        f.write(label.description)
        f.write("\n")
        print label.description
    f.close()
        
    